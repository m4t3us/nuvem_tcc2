import Vue from 'vue'
import Router from 'vue-router'
import Inicio from './views/Inicio.vue'
import Importar from './views/Importar.vue'
import Sobre from './views/Sobre.vue'
import Nuvem from './views/Nuvem.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'inicio',
      component: Inicio
    },
    {
      path: '/sobre',
      name: 'sobre',
      component: Sobre
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      //component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    },
    {
      path: '/importar',
      name: 'importar',
      component: Importar
    },
    {
      path: '/nuvem',
      name: 'nuvem',
      component: Nuvem
    },
  ]
})
