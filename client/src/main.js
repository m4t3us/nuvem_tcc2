import Vue from 'vue'
import App from './App.vue'
import router from './router'
import axios from 'axios'

import BootstrapVue from 'bootstrap-vue'

Vue.use(BootstrapVue)

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.config.productionTip = false
Vue.prototype.$http = axios
Vue.prototype.$api = "https://nuvem-tcc-api.herokuapp.com/api/"
//Vue.prototype.$api = "http://localhost:8000/api/"

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
