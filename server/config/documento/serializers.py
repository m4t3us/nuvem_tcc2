from rest_framework import serializers
from .models import Documento, Frequencia, FrequenciaDocumento,  StopWords

class DocumentoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Documento
        fields = '__all__'

class FrequenciaSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Frequencia
        fields = '__all__'

class FrequenciaDocumentoSerializer(serializers.ModelSerializer):
    frequencia = serializers.SerializerMethodField()
    documento = serializers.SerializerMethodField()
    anexo = serializers.SerializerMethodField()
    class Meta:
        model = FrequenciaDocumento
        fields = '__all__'

    def get_frequencia(self, obj):
        return "%s" % obj.frequencia.palavra

    def get_documento(self, obj):
        return "%s" % obj.documento.tema
    
    def get_anexo(self, obj):
        return "%s" % obj.documento.anexo

class StopWordsSerializer(serializers.ModelSerializer):
    class Meta:
        model = StopWords
        fields = '__all__'