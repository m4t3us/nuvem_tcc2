from django.contrib import admin
from .models import Documento, Frequencia, FrequenciaDocumento, StopWords
# Register your models here.
admin.site.register(Documento)
admin.site.register(Frequencia) 
admin.site.register(FrequenciaDocumento) 
admin.site.register(StopWords) 
