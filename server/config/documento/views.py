from rest_framework import viewsets, status
from .models import Documento, Frequencia, FrequenciaDocumento, StopWords
from .serializers import DocumentoSerializer, FrequenciaSerializer, FrequenciaDocumentoSerializer, StopWordsSerializer
from rest_framework.response import Response
from django.core.files.storage import FileSystemStorage
from django.conf import settings
import hashlib
from datetime import *
import datetime
import os
from tika import parser
import nltk
from unicodedata import *



class DocumentoViewSet(viewsets.ModelViewSet):
    queryset = Documento.objects.all()
    serializer_class = DocumentoSerializer


    def create(self, request):
            
        def pdf_to_txt(file):
            # Parse data from file
            file_data = parser.from_file(settings.MEDIA_ROOT +"/"+file)
            # Get files text content
            return file_data['content']


        def create_file_with_results(texto,file):
            f = open(file,'w')
            f.write( texto )
            f.close()

        def open_file_with_stopwords(file):
            f = open(settings.MEDIA_ROOT +"/"+ file,'r')
            content = f.read().split('\n')
            f.close()
            return content
            

        def sortSecond(val):
            return val[1]	



        retorno = {'msg':'','sucesso':False,'palavras':None}

        try:
            myfile = request.FILES['documento']
            fs = FileSystemStorage()
            filename = fs.save( datetime.datetime.now().strftime("%Y%m%d_%H%M%S") + '.pdf', myfile)
            uploaded_file_url = fs.url(filename)
            
            arq = open( settings.MEDIA_ROOT +"/"+ filename, 'rb' )
            arq_texto = arq.read()
            arq.close()

            h = hashlib.md5()
            h.update( arq_texto ) 
            hash = h.hexdigest()

            if Documento.objects.filter(hash=hash):
                if os.path.exists( settings.MEDIA_ROOT +"/"+ filename ):
                    os.remove( settings.MEDIA_ROOT +"/"+ filename )
                retorno['msg'] = 'Documento já inserido'
            else:
                documento = Documento()
                documento.anexo = uploaded_file_url
                documento.tema = request.POST['tema']
                documento.hash = hash
                
                
                stopwords_cadastradas = StopWords.objects.values_list('palavra',flat=True)
               
                #stopwords =  [u'...',u',',u'.',u':',u'-',u'(',u')',u'%',u'é',u'a',u';',u'ir',u'para'] + list(stopwords_cadastradas)
                tokens = nltk.word_tokenize(pdf_to_txt(filename))
                fd = nltk.FreqDist(w.lower() for w in tokens)
                
                lista = []
                documento.save()

                for word in list(fd.keys()):
                    if fd[word] >= 20 and len(word) > 2 and word not in list(stopwords_cadastradas):
                        lista.append( {'palavra': word ,'quantidade' :fd[word] } )

                        try:
                            frequencia = Frequencia.objects.get(palavra=word)
                            frequencia.quantidade = frequencia.quantidade + int(fd[word])
                            frequencia.save() 
                        except:
                            frequencia = Frequencia()
                            frequencia.palavra = word
                            frequencia.quantidade = int(fd[word])
                            frequencia.save()

                        frequencia_documento = FrequenciaDocumento()
                        frequencia_documento.documento = documento
                        frequencia_documento.frequencia = frequencia
                        frequencia_documento.quantidade = int(fd[word])
                        frequencia_documento.save()
                        #frequencia_documento.frequencia.add( frequencia )
                        

                #lista.sort( key = sortSecond , reverse=True)
                
                retorno['palavras'] = lista
                retorno['msg'] = 'Documento inserido com sucesso'
                retorno['sucesso'] = True

            return Response( retorno ,  status=200)

        except:
            retorno['msg'] = 'BadRequest'
            return Response(retorno , status=400)


class FrequenciaViewSet(viewsets.ModelViewSet):
    queryset = Frequencia.objects.all()
    serializer_class = FrequenciaSerializer

class FrequenciaDocumentoViewSet(viewsets.ModelViewSet):
    queryset = FrequenciaDocumento.objects.all()
    serializer_class = FrequenciaDocumentoSerializer

    def list(self, request):
        retorno = {'sucesso':False,'msg':'','documentos':None}
        palavra = request.GET['palavra']


        frequencia = Frequencia.objects.get(palavra=palavra)
        frequencia_id = frequencia.id

        frequenciadocumento = FrequenciaDocumento.objects.filter(frequencia=frequencia_id)
        
        serializer = self.get_serializer(frequenciadocumento, many=True)

        return Response(serializer.data, status=200)
    
class StopWordsViewSet(viewsets.ModelViewSet):
    queryset = StopWords.objects.all()
    serializer_class = StopWordsSerializer

    def create(self, request):
        retorno = {'msg':'','sucesso':False}
        try:
            count = 0
            palavras = request.data['palavra'].split('\n')
            for p in palavras:
                try:
                    stopword = StopWords.objects.get(palavra=p.strip())
                except:
                    stopword = StopWords()
                    stopword.palavra = p.strip()
                    stopword.save()
                    count = count + 1
            retorno['sucesso'] = True
            retorno['msg'] = 'Foram inseridas '+str(count)+' stopwrods'
            return Response( retorno , status=200)
        except:
            retorno['msg'] = 'BadRequest'
            return Response(retorno , status=400)

    
    