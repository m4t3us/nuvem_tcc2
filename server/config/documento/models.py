from django.db import models

# Create your models here.
class Documento(models.Model):
    anexo = models.CharField(max_length=350)
    tema = models.CharField(max_length=350)
    hash = models.CharField(max_length=450,unique=True)

    #def __str__(self):
    #    return self.tema

class Frequencia(models.Model):
    palavra = models.CharField(max_length=450,unique=True)
    quantidade = models.IntegerField()

    #def __str__(self):
    #     return self.palavra

class FrequenciaDocumento(models.Model):
    documento = models.ForeignKey(Documento, on_delete=models.CASCADE, null=True, blank=True)
    frequencia = models.ForeignKey(Frequencia, on_delete=models.CASCADE, null=True, blank=True)
    quantidade = models.IntegerField(null=True, blank=True)
    #documento = models.ManyToManyField(Documento)
    #frequencia = models.ManyToManyField(Frequencia)

    #def __str__(self):
    #    return self.documento.tema + ' - ' + int(self.frequencia.palavra)     

class StopWords(models.Model):
    palavra = models.TextField(unique=True)