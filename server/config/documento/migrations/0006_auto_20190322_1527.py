# Generated by Django 2.1.7 on 2019-03-22 18:27

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('documento', '0005_auto_20190322_1506'),
    ]

    operations = [
        migrations.AddField(
            model_name='frequenciadocumento',
            name='quantidade',
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.RemoveField(
            model_name='frequenciadocumento',
            name='frequencia',
        ),
        migrations.AddField(
            model_name='frequenciadocumento',
            name='frequencia',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='documento.Frequencia'),
        ),
    ]
