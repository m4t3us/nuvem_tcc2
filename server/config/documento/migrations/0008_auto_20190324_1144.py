# Generated by Django 2.1.7 on 2019-03-24 14:44

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('documento', '0007_auto_20190323_1724'),
    ]

    operations = [
        migrations.CreateModel(
            name='StopWords',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('palavra', models.CharField(max_length=450, unique=True)),
            ],
        ),
        migrations.AlterField(
            model_name='frequenciadocumento',
            name='documento',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='documento.Documento'),
        ),
    ]
