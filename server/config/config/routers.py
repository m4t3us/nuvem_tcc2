from rest_framework import routers
from documento.views import DocumentoViewSet, FrequenciaViewSet, FrequenciaDocumentoViewSet, StopWordsViewSet
router = routers.DefaultRouter()
router.register('documento', DocumentoViewSet)
router.register('frequencia', FrequenciaViewSet)
router.register('frequenciadocumento', FrequenciaDocumentoViewSet)
router.register('stopwords', StopWordsViewSet)